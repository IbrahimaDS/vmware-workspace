BACKUP_DIR=/home/vmware/backup/tkg-extensions-v1.1.0/authentication
LIVE_DIR=/home/vmware/workspace/tkg-extensions-v1.1.0/authentication
REF_DIR=/home/vmware/reference/tkg-extensions-v1.1.0/authentication

mkdir -p $BACKUP_DIR
mkdir -p $LIVE_DIR

rm -rf $BACKUP_DIR
mv $LIVE_DIR $BACKUP_DIR
cp -r $REF_DIR $LIVE_DIR

